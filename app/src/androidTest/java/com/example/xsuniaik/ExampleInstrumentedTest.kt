package com.example.xsuniaik

import androidx.compose.material.MaterialTheme
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.xsuniaik.communication.MockRemoteRepository
import com.example.xsuniaik.communication.db.MockLocalRepository
import com.example.xsuniaik.ml.MockMachineLearningModelImpl
import com.example.xsuniaik.navigation.Destination
import com.example.xsuniaik.navigation.NavGraph
import com.example.xsuniaik.ui.activities.MainActivity
import com.example.xsuniaik.ui.screens.auth.AuthScreen
import com.example.xsuniaik.ui.screens.auth.AuthScreenViewModel
import com.example.xsuniaik.ui.screens.detail.RecipeDetailViewModel
import com.example.xsuniaik.ui.screens.favorite.FavoriteScreen
import com.example.xsuniaik.ui.screens.favorite.FavoriteScreenViewModel
import com.example.xsuniaik.ui.screens.filteredRecipe.FilteredScreenViewModel
import com.example.xsuniaik.ui.screens.loupe.LoupeScreenViewModel
import com.example.xsuniaik.ui.screens.main.MainScreenViewModel
import com.example.xsuniaik.ui.screens.map.MapViewModel
import com.example.xsuniaik.utils.Constants.TT_BACK_BUTTON
import com.example.xsuniaik.utils.Constants.TT_FAVORITE_LIST
import com.example.xsuniaik.utils.Constants.TT_FILTERED_LIST
import com.example.xsuniaik.utils.Constants.TT_MAIN_LIST
import com.example.xsuniaik.utils.Constants.TT_MAP_BUTTON
import org.junit.Assert

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @get:Rule
    val composeRule = createAndroidComposeRule<MainActivity>()

    private lateinit var navController: NavHostController

    private val testModule = module {
        viewModel { RecipeDetailViewModel(MockLocalRepository()) }
        viewModel { FilteredScreenViewModel(MockLocalRepository()) }
        viewModel { MapViewModel(MockRemoteRepository()) }
        viewModel { AuthScreenViewModel() }
        viewModel { MainScreenViewModel(MockRemoteRepository()) }
        viewModel { FavoriteScreenViewModel(MockLocalRepository()) }
        viewModel { LoupeScreenViewModel(MockMachineLearningModelImpl())}
    }

    @Before
    fun setUp() {
        loadKoinModules(testModule)
    }
    
    
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.xsuniaik", appContext.packageName)
    }

    @Test
    fun test_favorite_screen_list_all_favorite_recipes() {
        //given
        launchReportScreenWithNavigation(Destination.FavoriteScreen.route)
        val itemsInDB = MockLocalRepository().getAllFavoriteRecipes("333").value!!.size

        //when + then
        with(composeRule) {
            onNodeWithTag(TT_FAVORITE_LIST).onChildren().assertCountEquals(itemsInDB)
        }
    }

    @Test
    fun test_filtered_screen_list_all_local_recipes() {
        //given
        launchReportScreenWithNavigation(Destination.FilteredScreen.route + "/{product}")
        val itemsInDB = MockLocalRepository().getAllFilteredRecipes().value!!.size

        //when + then
        with(composeRule) {
            onNodeWithTag(TT_FILTERED_LIST).onChildren().assertCountEquals(itemsInDB)
        }
    }

    @Test
    fun test_favorite_screen_navigates_to_recipe_detail_screen() {
        //given
        launchReportScreenWithNavigation(Destination.FavoriteScreen.route)

        with(composeRule) {
            //when
            onNodeWithTag(TT_FAVORITE_LIST).onChildren().onFirst().performClick()

            //then
            Assert.assertEquals(
                Destination.RecipeDetailScreen.route + "/{id}",
                currentBackStackEntry()
            )
        }
    }

    @Test
    fun test_main_screen_navigates_to_recipe_detail_screen() {
        //given
        launchReportScreenWithNavigation(Destination.MainScreen.route)

        with(composeRule) {
            //when
            onNodeWithTag(TT_MAIN_LIST).onChildren().onFirst().performClick()

            //then
            Assert.assertEquals(
                Destination.RecipeDetailScreen.route + "/{id}",
                currentBackStackEntry()
            )
        }
    }

    @Test
    fun test_filtered_screen_navigates_to_recipe_detail_screen() {
        //given
        launchReportScreenWithNavigation(Destination.FilteredScreen.route + "/{product}")

        with(composeRule) {
            //when
            onNodeWithTag(TT_FILTERED_LIST).onChildren().onFirst().performClick()

            //then
            Assert.assertEquals(
                Destination.RecipeDetailScreen.route + "/{id}",
                currentBackStackEntry()
            )
        }
    }



    @Test
    fun test_navigate_back_to_all_recipe_from_detail_recipe() {
        launchReportScreenWithNavigation(Destination.MainScreen.route)

        with(composeRule) {
            onNodeWithTag(TT_MAIN_LIST).onChildren().onFirst().performClick()
            waitForIdle()
            onNodeWithTag(TT_BACK_BUTTON).assertIsDisplayed()
            onNodeWithTag(TT_BACK_BUTTON).performClick()
            waitForIdle()

            val route = navController.currentBackStackEntry?.destination?.route
            assertTrue(route == Destination.MainScreen.route)
        }
    }

    private fun currentBackStackEntry(): String? {
        return navController.currentBackStackEntry?.destination?.route
    }

    private fun launchReportScreenWithNavigation(route:String) {
        composeRule.setContent {
            MaterialTheme {
                navController = rememberNavController()
                NavGraph(startDestination = route, navController = navController)
            }
        }
    }
}