package com.example.xsuniaik.communication.db

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.example.xsuniaik.models.local.LocalRecipe
import java.time.LocalDateTime
import java.time.ZoneOffset

class MockLocalRepository: IProjectLocalRepository {

    private var UT_FAVORITE_ITEM: FavoriteRecipe = FavoriteRecipe(
    2,
        userId = "333",
    name = "Chicken",
    cookTimeMinutes = 30,
    instractions = emptyList(),
    thumbnailUrl = "",
    sections =emptyList()
    )

    private var UT_LOCAL_ITEM: LocalRecipe = LocalRecipe(
        2,
        name = "Chicken",
        cookTimeMinutes = 30,
        instractions = emptyList(),
        thumbnailUrl = "",
        sections =emptyList()
    )

    private var localDB = mutableStateListOf(
        LocalRecipe(
            1,
            name = "Potato",
            cookTimeMinutes = 25,
            instractions = arrayListOf("salt"),
            thumbnailUrl = "",
            sections =arrayListOf("salt")
        ), LocalRecipe(
            2,
            name = "Chicken",
            cookTimeMinutes = 30,
            instractions = arrayListOf("salt"),
            thumbnailUrl = "",
            sections =arrayListOf("salt")
        )
    )
    private var favoriteLocalDB = mutableStateListOf(
        FavoriteRecipe(
            1,
            userId = "333",
            name = "Potato",
            cookTimeMinutes = 25,
            instractions = arrayListOf("salt"),
            thumbnailUrl = "",
            sections =arrayListOf("salt")
        ), FavoriteRecipe(
            2,
            userId = "333",
            name = "Chicken",
            cookTimeMinutes = 30,
            instractions = arrayListOf("salt"),
            thumbnailUrl = "",
            sections =arrayListOf("salt")
        )
    )


    override suspend fun getRecipeById(id: Long): LocalRecipe  = UT_LOCAL_ITEM

    override fun insertAllRecipe(recipes: List<LocalRecipe>): List<Long> {
        return listOf(12,324)
    }

    override fun getAllFavoriteRecipes(userId: String): LiveData<MutableList<FavoriteRecipe>> {
        return MutableLiveData(favoriteLocalDB)
    }

    override suspend fun getFavoriteRecipeById(id: Long): FavoriteRecipe = UT_FAVORITE_ITEM

    override suspend fun insertFavoriteRecipe(favoriteRecipe: FavoriteRecipe): Long {
        return 12
    }

    override suspend fun deleteFavoriteRecipe(favoriteRecipe: FavoriteRecipe) {
    }

    override fun getAllFilteredRecipes(): LiveData<MutableList<LocalRecipe>> {
        return MutableLiveData(localDB)
    }
}