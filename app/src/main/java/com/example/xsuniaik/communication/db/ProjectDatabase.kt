package com.example.xsuniaik.communication.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.example.xsuniaik.models.local.LocalRecipe
import com.example.xsuniaik.models.local.StringTypeConverter

@Database(entities = [LocalRecipe::class, FavoriteRecipe::class], version = 7, exportSchema = true)
@TypeConverters(StringTypeConverter::class)
abstract class ProjectDatabase : RoomDatabase() {
    abstract fun localRecipeDao(): LocalRecipeDAO
    abstract fun favoriteRecipeDao(): FavoriteRecipeDAO

    companion object {
        @Volatile
        private var INSTANCE: ProjectDatabase? = null

        fun getDatabase(context: Context): ProjectDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext, ProjectDatabase::class.java, "database"
        ).allowMainThreadQueries().addCallback(object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }).build()
    }
}