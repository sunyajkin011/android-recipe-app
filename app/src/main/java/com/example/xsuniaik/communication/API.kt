package com.example.xsuniaik.communication

import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.Store
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import retrofit2.http.Url

interface API {

    @Headers("X-RapidAPI-Key: 286fa27844msh16475c37a4ac26ep1b47ebjsnea58f4187b99",
        "X-RapidAPI-Host: tasty.p.rapidapi.com")
    @GET("recipes/list")
    suspend fun getAllRecipes(@Query("from") from: Int = 0,
                               @Query("size") size: Int = 100,
                               @Query("tags") tags: String = "under_30_minutes",): Response<ApiResponse>

    @Headers("Content-Type: application/json")
    @GET
    suspend fun getStores(@Url url: String = "https://6366948ff5f549f052c92ae4.mockapi.io/api/v1/stores/"): Response<List<Store>>
}
