package com.example.xsuniaik.communication.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xsuniaik.models.local.FavoriteRecipe

@Dao
interface FavoriteRecipeDAO {

    @Query("SELECT * FROM FavoriteRecipe WHERE user_id = :userId")
    fun getAll(userId: String): LiveData<MutableList<FavoriteRecipe>>

    @Query("SELECT * FROM FavoriteRecipe WHERE id = :id")
    suspend fun findById(id: Long): FavoriteRecipe

    @Insert
    suspend fun insertFavoriteRecipe(favoriteRecipe: FavoriteRecipe): Long

    @Delete
    suspend fun deleteFavoriteRecipe(favoriteRecipe: FavoriteRecipe)


}