package com.example.xsuniaik.communication

import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.ktx.Firebase
import com.google.firebase.firestore.ktx.firestore



const val DETAILS_COLLECTION_REF = "details"

class FireBaseRepository(){

    val user = Firebase.auth.currentUser


    private val detailsRef: CollectionReference = Firebase.firestore.collection(
        DETAILS_COLLECTION_REF)
}

sealed class Resources<T>(
    val data: T? = null,
    val throwable: Throwable? = null,
){
    class Loading<T>: Resources<T>()
    class Success<T>(data: T?): Resources<T>(data = data)
    class Error<T>(throwable: Throwable?):Resources<T>(throwable = throwable)
}