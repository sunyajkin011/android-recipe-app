package com.example.xsuniaik.communication

import com.example.xsuniaik.architecture.CommunicationResult
import com.example.xsuniaik.architecture.IBaseRemoteRepository
import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.Store

interface IRemoteRepository : IBaseRemoteRepository {

    suspend fun getAllRecipes(): CommunicationResult<ApiResponse>

    suspend fun getStores(): CommunicationResult<List<Store>>

}