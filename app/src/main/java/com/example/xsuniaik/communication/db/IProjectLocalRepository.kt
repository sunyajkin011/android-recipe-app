package com.example.xsuniaik.communication.db

import androidx.lifecycle.LiveData
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.example.xsuniaik.models.local.LocalRecipe

interface IProjectLocalRepository {
    suspend fun getRecipeById(id: Long): LocalRecipe
    fun insertAllRecipe(recipes: List<LocalRecipe>): List<Long>
    fun getAllFavoriteRecipes(userId: String): LiveData<MutableList<FavoriteRecipe>>
    suspend fun getFavoriteRecipeById(id: Long): FavoriteRecipe
    suspend fun insertFavoriteRecipe(favoriteRecipe: FavoriteRecipe): Long
    suspend fun deleteFavoriteRecipe(favoriteRecipe: FavoriteRecipe)
     fun getAllFilteredRecipes(): LiveData<MutableList<LocalRecipe>>
}