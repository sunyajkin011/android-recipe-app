package com.example.xsuniaik.communication.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xsuniaik.models.local.LocalRecipe


@Dao
interface LocalRecipeDAO {

    @Query("SELECT * FROM LocalRecipe")
    fun getAll(): LiveData<MutableList<LocalRecipe>>

    @Query("SELECT * FROM LocalRecipe WHERE id = :id")
    suspend fun findById(id: Long): LocalRecipe

    @Insert
    fun insertAll(recipes: List<LocalRecipe>): List<Long>


}