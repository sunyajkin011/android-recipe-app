package com.example.xsuniaik.communication.db

import androidx.compose.runtime.livedata.observeAsState
import androidx.lifecycle.LiveData
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.example.xsuniaik.models.local.LocalRecipe

class ProjectLocalRepositoryImpl(private val localRecipeDAO: LocalRecipeDAO, private val favoriteRecipeDAO: FavoriteRecipeDAO): IProjectLocalRepository {
    override suspend fun getRecipeById(id: Long): LocalRecipe {
        return localRecipeDAO.findById(id)
    }

    override fun insertAllRecipe(recipes: List<LocalRecipe>): List<Long> {
        return localRecipeDAO.insertAll(recipes)
    }

    override fun getAllFavoriteRecipes(userId: String): LiveData<MutableList<FavoriteRecipe>> {
        return  favoriteRecipeDAO.getAll(userId)
    }

    override suspend fun getFavoriteRecipeById(id: Long): FavoriteRecipe {
        return favoriteRecipeDAO.findById(id)
    }

    override suspend fun insertFavoriteRecipe(favoriteRecipe: FavoriteRecipe): Long {
        return favoriteRecipeDAO.insertFavoriteRecipe(favoriteRecipe = favoriteRecipe)
    }

    override suspend fun deleteFavoriteRecipe(favoriteRecipe: FavoriteRecipe) {
        return favoriteRecipeDAO.deleteFavoriteRecipe(favoriteRecipe)
    }

    override fun getAllFilteredRecipes(): LiveData<MutableList<LocalRecipe>> {
        return localRecipeDAO.getAll()
    }


}