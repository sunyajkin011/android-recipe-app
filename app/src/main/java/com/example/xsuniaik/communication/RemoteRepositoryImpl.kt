package com.example.xsuniaik.communication

import com.example.xsuniaik.architecture.CommunicationError
import com.example.xsuniaik.architecture.CommunicationResult
import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.Store
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class RemoteRepositoryImpl(private val api: API) : IRemoteRepository {

    override suspend fun getAllRecipes(): CommunicationResult<ApiResponse> {
        return try {
            processResponse(withContext(Dispatchers.IO){api.getAllRecipes()})
        } catch (timeoutEx: SocketTimeoutException) {
            CommunicationResult.Error(CommunicationError(0, "Timeout"))
        } catch (unknownHostEx: UnknownHostException) {
            CommunicationResult.Error(CommunicationError(0, "Unknown host"))
        }
    }

    override suspend fun getStores(): CommunicationResult<List<Store>> {
        return try {
            processResponse(withContext(Dispatchers.IO){api.getStores()})
        } catch (timeoutEx: SocketTimeoutException) {
            CommunicationResult.Error(CommunicationError(0, "Timeout"))
        } catch (unknownHostEx: UnknownHostException) {
            CommunicationResult.Error(CommunicationError(0, "Unknown host"))
        }
    }


}