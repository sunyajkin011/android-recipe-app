package com.example.xsuniaik.communication

import com.example.xsuniaik.architecture.CommunicationResult
import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.Store
import com.example.xsuniaik.models.local.LocalRecipe

class MockRemoteRepository: IRemoteRepository {

    private val API_RESONSE = ApiResponse(
        count = 2,
        results = listOf(
            Recipe(
                1,
                name = "Potato",
                cook_time_minutes = 25,
                instractions = emptyList(),
                thumbnail_url = "",
                sections =emptyList()
            ), Recipe(
                2,
                name = "Chicken",
                cook_time_minutes = 30,
                instractions = emptyList(),
                thumbnail_url = "",
                sections =emptyList()
            )
        )
    )

    private val API_STORE_RESPONSE = listOf(
        Store(
            1,
            name = "Sturbaks",
            address = "Kohoutova 9",
            latitude = 23.2,
            longitude = 56.9,
            type = "food"
        ), Store(
            2,
            name = "Sturbaks",
            address = "Namesti Svobody 17",
            latitude = 23.4,
            longitude = 56.5,
            type = "food"
        )
    )
    override suspend fun getAllRecipes(): CommunicationResult<ApiResponse> {
        return CommunicationResult.Success(API_RESONSE)
    }

    override suspend fun getStores(): CommunicationResult<List<Store>> {
        return CommunicationResult.Success(API_STORE_RESPONSE)
    }
}