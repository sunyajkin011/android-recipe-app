package com.example.xsuniaik.di

import com.example.xsuniaik.communication.db.FavoriteRecipeDAO
import com.example.xsuniaik.communication.db.LocalRecipeDAO
import com.example.xsuniaik.communication.db.ProjectDatabase
import org.koin.dsl.module

val daoModule = module {
    single { provideLocalRecipeDao(get()) }
    single { provideFavoriteRecipeDao(get()) }
}

fun provideLocalRecipeDao(db: ProjectDatabase): LocalRecipeDAO = db.localRecipeDao()
fun provideFavoriteRecipeDao(db: ProjectDatabase): FavoriteRecipeDAO = db.favoriteRecipeDao()