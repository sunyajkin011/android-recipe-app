package com.example.xsuniaik.di

import android.content.Context
import com.example.xsuniaik.communication.db.ProjectDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module { single { provideDatabase(androidContext()) } }

fun provideDatabase(context: Context): ProjectDatabase = ProjectDatabase.getDatabase(context)
