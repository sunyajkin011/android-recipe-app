package com.example.xsuniaik.di

import com.example.xsuniaik.communication.API
import com.example.xsuniaik.communication.IRemoteRepository
import com.example.xsuniaik.communication.RemoteRepositoryImpl
import com.example.xsuniaik.communication.db.FavoriteRecipeDAO
import com.example.xsuniaik.communication.db.IProjectLocalRepository
import com.example.xsuniaik.communication.db.LocalRecipeDAO
import com.example.xsuniaik.communication.db.ProjectLocalRepositoryImpl
import org.koin.dsl.module

val remoteRepositoryModule = module {
    single { provideRemoteRepository(get()) }
    single { provideLocalRepository(get(), get()) }
}

fun provideRemoteRepository(api: API): IRemoteRepository =
    RemoteRepositoryImpl(api)

fun provideLocalRepository(dao1: LocalRecipeDAO, dao2: FavoriteRecipeDAO):
    IProjectLocalRepository = ProjectLocalRepositoryImpl(dao1, dao2)


