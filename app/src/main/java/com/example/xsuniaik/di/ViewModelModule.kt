package com.example.xsuniaik.di

import com.example.xsuniaik.ui.screens.auth.AuthScreenViewModel
import com.example.xsuniaik.ui.screens.loupe.LoupeScreenViewModel
import com.example.xsuniaik.ui.screens.main.MainScreenViewModel
import com.example.xsuniaik.ui.screens.detail.RecipeDetailViewModel
import com.example.xsuniaik.ui.screens.favorite.FavoriteScreen
import com.example.xsuniaik.ui.screens.favorite.FavoriteScreenViewModel
import com.example.xsuniaik.ui.screens.filteredRecipe.FilteredScreenViewModel
import com.example.xsuniaik.ui.screens.map.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        AuthScreenViewModel()
    }

    viewModel {
        MainScreenViewModel(get())
    }

    viewModel {
        RecipeDetailViewModel(get())
    }

    viewModel {
        LoupeScreenViewModel(get())
    }
    viewModel {
        MapViewModel(get())
    }

    viewModel {
        FavoriteScreenViewModel(get())
    }

    viewModel {
        FilteredScreenViewModel(get())
    }



}