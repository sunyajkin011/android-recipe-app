package com.example.xsuniaik.di

import com.example.xsuniaik.ml.IMachineLearningModel
import com.example.xsuniaik.ml.MachineLearningModelImpl
import org.koin.dsl.module

val mlModule = module { single { provideMachineLearningModel() } }

fun provideMachineLearningModel(): IMachineLearningModel = MachineLearningModelImpl()