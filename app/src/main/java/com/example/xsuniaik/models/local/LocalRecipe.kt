package com.example.xsuniaik.models.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "LocalRecipe")
class LocalRecipe(
    @PrimaryKey
    @ColumnInfo(name = "id") var id: Long = 0,
    @ColumnInfo(name = "cook_time_minutes") var cookTimeMinutes: Int?,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "instractions") var instractions: List<String>?,
    @ColumnInfo(name = "thumbnail_url") var thumbnailUrl: String,
    @ColumnInfo(name = "sections") var sections: List<String>?,
) {



    constructor(id: Long, name: String, cook_time_minutes: Int?, instractions: List<String>?, thumbnail_url: String, sections: List<String>?) : this(
        id=id,
        cookTimeMinutes = cook_time_minutes,
        name = name,
        instractions = instractions,
        thumbnailUrl = thumbnail_url,
        sections = sections
    ){
        this.id = id
    }
}






