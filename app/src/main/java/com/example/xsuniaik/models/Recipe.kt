package com.example.xsuniaik.models


data class Recipe (
    var id: Int,
    var cook_time_minutes: Int?,
    var name: String,
    var instractions: List<Instruction>?,
    var thumbnail_url: String,
    var sections: List<Item>?
    )

