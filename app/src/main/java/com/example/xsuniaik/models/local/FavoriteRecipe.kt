package com.example.xsuniaik.models.local

import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@Entity(tableName = "FavoriteRecipe")
class FavoriteRecipe(
    @PrimaryKey @ColumnInfo(name = "id") var id: Long = 0,
    @ColumnInfo(name = "user_id") var userId: String,
    @ColumnInfo(name = "cook_time_minutes") var cookTimeMinutes: Int?,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "instractions") var instractions: List<String>?,
    @ColumnInfo(name = "thumbnail_url") var thumbnailUrl: String,
    @ColumnInfo(name = "sections") var sections: List<String>?,
) {

    constructor(id: Long,cook_time_minutes: Int?, userId: String, name: String, instractions: List<String>?, thumbnail_url: String, sections: List<String>?) : this(
        id =id,
        userId = userId,
        cookTimeMinutes = cook_time_minutes,
        name = name,
        instractions = instractions,
        thumbnailUrl = thumbnail_url,
        sections = sections
    )
}

class StringTypeConverter {
    @TypeConverter
    @Nullable
    fun fromString(value: String?): List<String>? {

        val listType =object : TypeToken<List<String>>(){}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @Nullable
    fun frmList(list: List<String>?): String {
        return  Gson().toJson(list)
    }

}
