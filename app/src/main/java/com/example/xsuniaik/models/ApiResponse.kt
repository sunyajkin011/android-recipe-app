package com.example.xsuniaik.models

data class ApiResponse (
    var count: Int,
    var results: List<Recipe>
        )