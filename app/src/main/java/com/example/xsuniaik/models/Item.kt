package com.example.xsuniaik.models


data class Item (
  var components: List<Component>
)
