package com.example.xsuniaik.models

import com.example.xsuniaik.R
import com.example.xsuniaik.utils.Constants.TT_HEART_BOTTOM_ITEM
import com.example.xsuniaik.utils.Constants.TT_HOME_BOTTOM_ITEM
import com.example.xsuniaik.utils.Constants.TT_LOUPE_BOTTOM_ITEM

sealed class NavigationItems(var route: String, var icon: Int, var title: String, var testTag: String)
{
    object Home : NavigationItems("home", R.drawable.ic_home, "Home", TT_HOME_BOTTOM_ITEM)
    object Loupe : NavigationItems("loupe", R.drawable.ic_loupe, "Loupe", TT_LOUPE_BOTTOM_ITEM)
    object Heart : NavigationItems("heart", R.drawable.ic_heart, "Heart", TT_HEART_BOTTOM_ITEM)
}
