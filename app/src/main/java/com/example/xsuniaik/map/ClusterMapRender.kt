package com.example.xsuniaik.map


import android.content.Context
import android.graphics.Bitmap
import com.example.xsuniaik.R
import com.example.xsuniaik.models.Store
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer

class ClusterMapRender(
    val context: Context, map: GoogleMap,
    clusterManager: ClusterManager<Store>,
) : DefaultClusterRenderer<Store>(context, map, clusterManager) {

    private val icons: MutableMap<Int, Bitmap> = mutableMapOf()

    override fun onBeforeClusterItemRendered(item: Store, markerOptions: MarkerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions)

        if (!icons.containsKey(item.id)) {
            when (item.type) {
                "food" -> icons.put(
                    item.id,
                    MarkerUtil.createMarkerIconFromResource(context, R.drawable.ic_food)
                )
                "clothes" -> icons.put(
                    item.id,
                    MarkerUtil.createMarkerIconFromResource(context, R.drawable.ic_food)
                )
                "kids" -> icons.put(
                    item.id,
                    MarkerUtil.createMarkerIconFromResource(context, R.drawable.ic_food)
                )
                "electronics" -> icons.put(
                    item.id,
                    MarkerUtil.createMarkerIconFromResource(context, R.drawable.ic_food)
                )
                "fast_food" -> icons.put(
                    item.id,
                    MarkerUtil.createMarkerIconFromResource(context, R.drawable.ic_food)
                )
            }
        }

        markerOptions.icon(
            BitmapDescriptorFactory
                .fromBitmap(icons[item.id]!!))
    }

    override fun shouldRenderAsCluster(cluster: Cluster<Store>): Boolean {
        return cluster.size > 5
    }
}