package com.example.xsuniaik.utils

import android.content.Context
import androidx.compose.runtime.collectAsState
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

class DataStore(private val context: Context) {
    companion object {
        private val Context.userPreferencesDataStore: DataStore<Preferences> by preferencesDataStore("UserEmail")
        val USER_EMAIL = stringPreferencesKey("user_email")
    }

    fun getEmail() = context.userPreferencesDataStore.data
        .map { it[USER_EMAIL] ?: "0" }
        .flowOn(Dispatchers.IO)


    suspend fun saveEmail(count: String) =
        context.userPreferencesDataStore.edit { it[USER_EMAIL] = count }
}