package com.example.xsuniaik.utils

object Constants {
    const val ID = "id"
    const val PRODUCT = "product"
    const val mlModel = "mobilnet.tflite"


    const val TT_LOUPE_BOTTOM_ITEM = "loupe_bottom_item"
    const val TT_HOME_BOTTOM_ITEM = "home_bottom_item"
    const val TT_HEART_BOTTOM_ITEM = "heart_bottom_item"
    const val TT_PICK_PHOTO = "pick_photo"
    const val TT_ANALYZE_BUTTON = "analyze_button"
    const val TT_DETECTED_OBJECTS = "detected_objects"
    const val TT_BACK_BUTTON = "back_button"
    const val TT_HEART_BUTTON = "heart_button"
    const val TT_LOGOUT_BUTTON = "logout_button"
    const val TT_FAVORITE_LIST = "favorite_list"
    const val TT_FILTERED_LIST = "filtered_list"
    const val TT_MAIN_LIST = "main_list"
    const val TT_MAP_BUTTON = "map_button"
}