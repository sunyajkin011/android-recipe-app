package com.example.xsuniaik.ui.screens.map

sealed class MapUiState<out T> {
    class Start() : MapUiState<Nothing>()
    class Success<T>(var data: T) : MapUiState<T>()
    class Error(var error: Int) : MapUiState<Nothing>()
}