package com.example.xsuniaik.ui.screens.main

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.xsuniaik.R
import com.example.xsuniaik.architecture.BaseViewModel
import com.example.xsuniaik.architecture.CommunicationResult
import com.example.xsuniaik.communication.IRemoteRepository
import com.example.xsuniaik.communication.RemoteRepositoryImpl
import com.example.xsuniaik.communication.db.ProjectLocalRepositoryImpl
import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.local.LocalRecipe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.streams.toList

class MainScreenViewModel(
    private val repo: IRemoteRepository) : BaseViewModel() {


    val mainScreenlUIState: MutableState<MainScreenUIState<ApiResponse>> =
        mutableStateOf(MainScreenUIState.Start())


    fun loadRecipes(){
        launch {
            val result = withContext(Dispatchers.IO){
                repo.getAllRecipes()
            }

            when(result){
                is CommunicationResult.Error -> {
                    mainScreenlUIState.value = MainScreenUIState.Error(R.string.failed)
                }
                is CommunicationResult.Exception -> {
                    mainScreenlUIState.value =
                        MainScreenUIState.Error(R.string.no_internet_connection)
                }
                is CommunicationResult.Success -> {
                    mainScreenlUIState.value = MainScreenUIState.Success(result.data)
                   // localRepo.insertAllRecipe(tranformToLocalRecipe(result.data.results))
                }
            }
        }
    }

    fun tranformToLocalRecipe(results: List<Recipe>): List<LocalRecipe> {
        val list = arrayListOf<LocalRecipe>()
        results.forEach{
            var instctions: List<String>? = null
            if(it.instractions != null){
                instctions = it.instractions!!.stream().map {it.display_text }.toList()
            }
            var components: List<String>? = null
            if(it.sections != null){
                components = it.sections!!.stream().map { it.components }.flatMap {it.toList().stream()}.map { it.raw_text }.toList()
            }
            list.add(LocalRecipe(
                it.id.toLong(),
                it.cook_time_minutes,
                it.name,
                instctions,
                it.thumbnail_url,
                components
            ))
        }
        return list
    }
}