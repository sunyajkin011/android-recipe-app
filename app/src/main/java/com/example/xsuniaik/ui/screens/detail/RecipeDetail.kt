package com.example.xsuniaik.ui.screens.detail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.xsuniaik.R
import com.example.xsuniaik.models.ScreenState
import com.example.xsuniaik.models.local.LocalRecipe
import com.example.xsuniaik.navigation.INavigationRouter
import com.example.xsuniaik.utils.Constants
import com.example.xsuniaik.utils.Constants.TT_HEART_BUTTON
import cz.mendelu.pef.compose.petstore.ui.elements.BackArrowScreen
import cz.mendelu.pef.compose.petstore.ui.elements.ErrorScreen
import cz.mendelu.pef.compose.petstore.ui.elements.LoadingScreen
import org.koin.androidx.compose.getViewModel

@Composable
fun RecipeDetail(
    navigation: INavigationRouter,
    id: Long,
    viewModel: RecipeDetailViewModel = getViewModel()
) {

    val screenState: MutableState<ScreenState<LocalRecipe>> = rememberSaveable {
        mutableStateOf(ScreenState.Loading)
    }

    viewModel.recipeDetailUIState.value.let {
        when (it) {
            is RecipeDetailUIState.Error -> {
                screenState.value = ScreenState.Error(it.error)
            }
            is RecipeDetailUIState.Start -> {
                LaunchedEffect(it) {
                    viewModel.loadRecipeDetail(id)
                }
            }
            is RecipeDetailUIState.Success -> {
                LaunchedEffect(it) {
                    screenState.value = ScreenState.DataLoaded(it.data)
                    viewModel.selectedLocalRecipe = it.data
                }
            }
        }
    }
    Scaffold(
        topBar = {
            BackArrowScreen(
                topBarText = stringResource(id = R.string.detail),
                onBackClick = { navigation.returnBack() },
                actions = {
                    IconButton(modifier = Modifier.testTag(TT_HEART_BUTTON), onClick = {
                        viewModel.addToFavorite()
                    }) {
                        Icon(
                            painterResource(id = R.drawable.ic_heart),
                            tint = Color.Black,
                            contentDescription = "",
                        )
                    }
                }
            )
        },
        content = {
            RecipeDetailScreenContent(
                screenState = screenState.value,
                navigation = navigation,
                viewModel = viewModel
            )
        }
    )
}


@Composable
fun RecipeDetailScreenContent(
    screenState: ScreenState<LocalRecipe>,
    navigation: INavigationRouter,
    viewModel: RecipeDetailViewModel
) {
    screenState.let {
        when (it) {
            is ScreenState.DataLoaded -> RecipeDetail(
                recipe = it.data,
                navigation = navigation,
                viewModel = viewModel,

                )
            is ScreenState.Error -> ErrorScreen(text = stringResource(id = it.error), navigation)
            is ScreenState.Loading -> LoadingScreen()
        }
    }
}

@Composable
fun RecipeDetail(
    recipe: LocalRecipe,
    navigation: INavigationRouter,
    viewModel: RecipeDetailViewModel
) {
    Column() {
        Box(
            modifier = Modifier.height(200.dp)
        ) {
            Image(
                painter = rememberAsyncImagePainter(
                    if (!recipe.thumbnailUrl.contains("http"))
                        "https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg"
                    else
                        recipe.thumbnailUrl
                ), contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            colors = listOf(
                                Color.Transparent,
                                Color.Black
                            ),
                            startY = 300f
                        )
                    )
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(12.dp),
                contentAlignment = Alignment.BottomStart
            ) {
                androidx.compose.material3.Text(
                    recipe.name,
                    style = TextStyle(color = Color.White, fontSize = 16.sp)
                )
            }
        }
        Column(modifier = Modifier.background(color = colorResource(id = R.color.select)).fillMaxHeight()) {


            ClickableText(
                modifier = Modifier.padding(8.dp).testTag(Constants.TT_MAP_BUTTON),
                text = AnnotatedString(stringResource(id = R.string.show_map)),
                onClick = {
                    navigation.navigateToMapScreen()
                },
            )

            LazyColumn(modifier = Modifier.fillMaxWidth()) {
                if (recipe.sections != null) {
                    recipe.sections!!.forEach {
                        item(key = it.hashCode()) {
                            Text(text = it, modifier = Modifier.padding(5.dp))
                        }
                    }
                }
            }

            LazyColumn(modifier = Modifier.fillMaxWidth()) {
                if (recipe.instractions != null) {
                    recipe.instractions!!.forEach {
                        item(key = it.hashCode()) {
                            Text(text = it)
                        }
                    }
                }
            }
        }
    }
}