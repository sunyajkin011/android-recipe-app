package com.example.xsuniaik.ui.screens.favorite

sealed class FavoriteScreenUIState<out T> {
    class Start() : FavoriteScreenUIState<Nothing>()
    class Success<T>(var data: T) : FavoriteScreenUIState<T>()
    class Error(var error: Int) : FavoriteScreenUIState<Nothing>()
}