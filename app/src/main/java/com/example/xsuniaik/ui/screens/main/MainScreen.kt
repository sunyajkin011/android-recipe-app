package com.example.xsuniaik.ui.screens.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.TopAppBar
import androidx.compose.material3.*
import androidx.compose.material3.CardElevation
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.xsuniaik.R
import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.ScreenState
import com.example.xsuniaik.navigation.INavigationRouter
import com.example.xsuniaik.utils.Constants
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.mendelu.pef.compose.petstore.ui.elements.ErrorScreen
import cz.mendelu.pef.compose.petstore.ui.elements.LoadingScreen
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(
    navigation: INavigationRouter,
    viewModel: MainScreenViewModel = getViewModel()
) {
    val screenState: MutableState<ScreenState<ApiResponse>> = rememberSaveable {
        mutableStateOf(ScreenState.Loading)
    }

    viewModel.mainScreenlUIState.value.let {
        when(it){
            is MainScreenUIState.Error -> {
                screenState.value = ScreenState.Error(it.error)
            }
            is MainScreenUIState.Start -> {
                LaunchedEffect(it){
                    viewModel.loadRecipes()
                }
            }
            is MainScreenUIState.Success -> {
                screenState.value = ScreenState.DataLoaded(it.data)
            }
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(align = Alignment.CenterHorizontally),
                        
                    ) {
                        Text(
                            text = stringResource(id = R.string.recipes),
                            style = MaterialTheme.typography.bodyMedium,
                            color = Color.Black,
                            modifier = Modifier
                                .padding(start = 0.dp)
                                .weight(1.5f)
                        )
                    }
                },
                actions = {
                    IconButton(modifier = Modifier.testTag(Constants.TT_LOGOUT_BUTTON),
                        onClick = {
                            Firebase.auth.signOut()
                            navigation.navigateToAuthScreen()
                        }) {
                        Icon(
                            painterResource(id = R.drawable.ic_logout),
                            tint = Color.Black,
                            contentDescription = "",
                        )
                    }
                },
                elevation = 0.dp,
                backgroundColor = colorResource(id = R.color.unselect)
            )
        },
        content = {
            PetsListScreenContent(
                paddingValues = it,
                navigation = navigation,
                screenState = screenState.value
            )
        }
    )

}

@Composable
fun PetsListScreenContent(
    paddingValues: PaddingValues,
    navigation: INavigationRouter,
    screenState: ScreenState<ApiResponse>){

    screenState.let {
        when(it){
            is ScreenState.DataLoaded -> PetsList(
                paddingValues = paddingValues,
                navigation = navigation,
                recipes = it.data.results
            )
            is ScreenState.Error -> ErrorScreen(text = stringResource(id = it.error), navigation)
            is ScreenState.Loading -> LoadingScreen()
        }


    }

}

@Composable
fun PetsList(paddingValues: PaddingValues,
             navigation: INavigationRouter,
             recipes: List<Recipe>){
    LazyColumn(modifier = Modifier.padding(paddingValues).testTag(Constants.TT_MAIN_LIST)) {
        recipes.forEach {
            item(key = it.name) {
                PetRow(
                    recipe = it,
                    onRowClick = {
                        navigation.navigateToRecipeDetail(it.id.toLong())
                    }
                )
            }
        }
    }
}

@Composable
fun PetRow(recipe: Recipe,
           onRowClick: () -> Unit){
Box(
    modifier = Modifier.padding(16.dp)
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(onClick = onRowClick),
        shape = RoundedCornerShape(15.dp),
        elevation = CardDefaults.cardElevation(5.dp)
    ) {
        Box(
            modifier = Modifier.height(200.dp)
        ) {
            Image(
                painter = rememberAsyncImagePainter(
                    if (!recipe.thumbnail_url.contains("http"))
                        "https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg"
                    else
                        recipe.thumbnail_url
                ), contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
            Box(modifier = Modifier
                .fillMaxSize()
                .background(
                    Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            Color.Black
                        ),
                        startY = 300f
                    )
                ))
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(12.dp),
                contentAlignment = Alignment.BottomStart
            ) {
                Text(recipe.name, style = TextStyle(color = Color.White, fontSize = 16.sp))
            }
        }
    }
}
}