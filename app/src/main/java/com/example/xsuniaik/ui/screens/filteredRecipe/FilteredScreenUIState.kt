package com.example.xsuniaik.ui.screens.filteredRecipe

sealed class FilteredScreenUIState<out T> {
    class Start() : FilteredScreenUIState<Nothing>()
    class Success<T>(var data: T) : FilteredScreenUIState<T>()
    class Error(var error: Int) : FilteredScreenUIState<Nothing>()
}