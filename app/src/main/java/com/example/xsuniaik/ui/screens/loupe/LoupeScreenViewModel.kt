package com.example.xsuniaik.ui.screens.loupe

import android.graphics.Bitmap
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.example.xsuniaik.architecture.BaseViewModel
import com.example.xsuniaik.ml.IMachineLearningModel
import com.google.mlkit.vision.label.ImageLabel

class LoupeScreenViewModel(private val mlModel: IMachineLearningModel) : BaseViewModel() {

    var detectedObjects by mutableStateOf<List<ImageLabel>>(emptyList())

    fun getImageLabels(bitmap: Bitmap?) {
        mlModel.processImage(bitmap!!).addOnSuccessListener { detectedObjects = it }
    }

}