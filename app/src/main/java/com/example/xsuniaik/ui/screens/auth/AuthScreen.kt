package com.example.xsuniaik.ui.screens.auth

import android.util.Patterns
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.*
import androidx.compose.material3.AlertDialogDefaults.containerColor
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.paint
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.xsuniaik.R
import com.example.xsuniaik.navigation.INavigationRouter
import com.example.xsuniaik.utils.DataStore
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AuthScreen(
    navigation: INavigationRouter,
    viewModel: AuthScreenViewModel = getViewModel()
) {
    val auth by lazy {
        Firebase.auth
    }

    val context = LocalContext.current
    val datastore = DataStore(context)
    val scope = rememberCoroutineScope()

    val emailDataStore  = DataStore(LocalContext.current).getEmail().collectAsState("")

    val focusManager = LocalFocusManager.current

    var email by remember{
        mutableStateOf("")
    }
    var password by remember{
        mutableStateOf("")
    }

    val isEmailValid by derivedStateOf {
        Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    val isPasswordValid by derivedStateOf {
        password.length > 7
    }
    var isPasswordVisible by remember {
        mutableStateOf(false)
    }
    email = emailDataStore.value



    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .paint(painterResource(id = R.drawable.back), contentScale = ContentScale.FillWidth),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 8.dp)
                .alpha(0.8f),
            shape = RoundedCornerShape(24.dp),
            color = Color.Black,
        ) {


            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(8.dp),
                modifier = Modifier.padding(all = 10.dp)
            ) {
                Text(
                    text = stringResource(R.string.hello),
                    color = Color.White,
                    fontFamily = FontFamily.SansSerif,
                    fontWeight = FontWeight.Bold,
                    fontStyle = FontStyle.Italic,
                    fontSize = 32.sp,
                    modifier = Modifier.padding(top = 6.dp)
                )
                Text(
                    text = stringResource(R.string.text1),
                    color = Color.White,
                    fontFamily = FontFamily.Companion.SansSerif,
                    fontWeight = FontWeight.Bold,
                    fontStyle = FontStyle.Italic,
                    fontSize = 16.sp,
                    modifier = Modifier.padding(top = 10.dp)
                )

Surface(
    color = Color.White,
    shape = RoundedCornerShape(24.dp)

) {
    OutlinedTextField(
        value = email,
        onValueChange = { email = it },
        label = { Text(stringResource(R.string.email_address)) },
        placeholder = { Text(emailDataStore.value) },
        singleLine = true,
        modifier = Modifier.fillMaxWidth(),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Email,
            imeAction = androidx.compose.ui.text.input.ImeAction.Next
        ),
        keyboardActions = KeyboardActions(
            onNext = { focusManager.moveFocus(FocusDirection.Down) }
        ),
        isError = !isEmailValid,
        trailingIcon = {
            if (email.isNotBlank()) {
                IconButton(onClick = { email = "" }) {
                    Icon(
                        imageVector = Icons.Filled.Clear,
                        contentDescription = "Clear email"
                    )
                }
            }
        }
    )
}

                Surface(
                    color = Color.White,
                    shape = RoundedCornerShape(24.dp)

                ) {
                    OutlinedTextField(
                        value = password,
                        onValueChange = { password = it },
                        label = { Text(stringResource(R.string.password)) },
                        singleLine = true,
                        modifier = Modifier.fillMaxWidth(),
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Password,
                            imeAction = androidx.compose.ui.text.input.ImeAction.Done
                        ),
                        keyboardActions = KeyboardActions(
                            onDone = { focusManager.clearFocus() }
                        ),
                        trailingIcon = {
                            IconButton(onClick = { isPasswordVisible = !isPasswordVisible }) {
                                Icon(
                                    imageVector = if (isPasswordVisible) Icons.Default.Visibility else Icons.Default.VisibilityOff,
                                    contentDescription = "Toggle password visibility"
                                )
                            }
                        },
                        visualTransformation = if (isPasswordVisible) VisualTransformation.None else PasswordVisualTransformation()
                    )
                }

                Button(
                    onClick = {
                        scope.launch {
                            datastore.saveEmail(email)
                        }
                              auth.signInWithEmailAndPassword(email,password)
                                  .addOnCompleteListener {
                                      if(it.isSuccessful){
                                        navigation.navigateToMainScreen()
                                      }
                                  }

                    },
                    modifier = Modifier.fillMaxWidth(),
                    colors = ButtonDefaults.buttonColors(colorResource(id = R.color.unselect)),
                    enabled = isEmailValid && isPasswordValid
                ) {
                    Text(
                        text = stringResource(R.string.login),
                        fontWeight = FontWeight.Bold,
                        color = Color.Black,
                        fontSize = 16.sp
                    )
                }

                Text(
                    text = stringResource(id = R.string.or),
                    color = Color.White,
                    fontFamily = FontFamily.Companion.SansSerif,
                    fontWeight = FontWeight.Bold,
                    fontStyle = FontStyle.Italic,
                    fontSize = 16.sp,
                    modifier = Modifier.padding(top = 3.dp)
                )

                Button(
                    onClick = {
                        scope.launch {
                            datastore.saveEmail(email)
                        }
                        auth.createUserWithEmailAndPassword(email, password)
                        navigation.navigateToMainScreen()
                    },
                    enabled = true,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(all = 16.dp),
                    colors = ButtonDefaults.buttonColors(colorResource(id = R.color.unselect)),

                    ) {
                    Text(
                        text = stringResource(R.string.register),
                        fontWeight = FontWeight.Bold,
                        color = Color.Black,
                        fontSize = 16.sp
                    )
                }

            }
        }
    }
}