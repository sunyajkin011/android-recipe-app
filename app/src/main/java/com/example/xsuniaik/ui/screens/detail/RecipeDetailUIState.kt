package com.example.xsuniaik.ui.screens.detail

sealed class RecipeDetailUIState<out T> {
    class Start() : RecipeDetailUIState<Nothing>()
    class Success<T>(var data: T) : RecipeDetailUIState<T>()
    class Error(var error: Int) : RecipeDetailUIState<Nothing>()
}