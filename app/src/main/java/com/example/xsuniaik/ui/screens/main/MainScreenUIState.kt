package com.example.xsuniaik.ui.screens.main

sealed class MainScreenUIState<out T> {
    class Start() : MainScreenUIState<Nothing>()
    class Success<T>(var data: T) : MainScreenUIState<T>()
    class Error(var error: Int) : MainScreenUIState<Nothing>()
}