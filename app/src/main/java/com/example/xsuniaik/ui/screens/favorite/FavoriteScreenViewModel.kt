package com.example.xsuniaik.ui.screens.favorite

import androidx.lifecycle.LiveData
import com.example.xsuniaik.architecture.BaseViewModel
import com.example.xsuniaik.communication.db.IProjectLocalRepository
import com.example.xsuniaik.communication.db.ProjectLocalRepositoryImpl
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class FavoriteScreenViewModel(
    private val localRepo: IProjectLocalRepository) : BaseViewModel() {


    fun getAllFavorite(): LiveData<MutableList<FavoriteRecipe>> = localRepo.getAllFavoriteRecipes(Firebase.auth.uid.toString())
    }