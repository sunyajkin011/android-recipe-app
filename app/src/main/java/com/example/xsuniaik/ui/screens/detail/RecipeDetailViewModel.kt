package com.example.xsuniaik.ui.screens.detail

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.xsuniaik.R
import com.example.xsuniaik.architecture.BaseViewModel
import com.example.xsuniaik.architecture.CommunicationResult
import com.example.xsuniaik.communication.RemoteRepositoryImpl
import com.example.xsuniaik.communication.db.IProjectLocalRepository
import com.example.xsuniaik.communication.db.ProjectLocalRepositoryImpl
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.example.xsuniaik.models.local.LocalRecipe
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class RecipeDetailViewModel(private val localRepo: IProjectLocalRepository) : BaseViewModel() {

    val recipeDetailUIState: MutableState<RecipeDetailUIState<LocalRecipe>> =
        mutableStateOf(RecipeDetailUIState.Start())

    var selectedLocalRecipe: LocalRecipe? = null


    fun loadRecipeDetail(id: Long){
        launch {
                    recipeDetailUIState.value =
                        withContext(Dispatchers.IO) { RecipeDetailUIState.Success(localRepo.getRecipeById(id))}
                }
    }
     fun addToFavorite(){
         var fr: FavoriteRecipe = FavoriteRecipe(
             selectedLocalRecipe!!.id,
             Firebase.auth.uid.toString(),
             selectedLocalRecipe!!.cookTimeMinutes,
             selectedLocalRecipe!!.name,
             selectedLocalRecipe!!.instractions,
             selectedLocalRecipe!!.thumbnailUrl,
             selectedLocalRecipe!!.sections
         )
        launch {
            localRepo.insertFavoriteRecipe(fr)
        }
    }
}