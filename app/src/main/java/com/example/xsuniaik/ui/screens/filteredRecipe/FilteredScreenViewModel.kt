package com.example.xsuniaik.ui.screens.filteredRecipe


import androidx.lifecycle.LiveData
import com.example.xsuniaik.architecture.BaseViewModel
import com.example.xsuniaik.communication.RemoteRepositoryImpl
import com.example.xsuniaik.communication.db.IProjectLocalRepository
import com.example.xsuniaik.communication.db.ProjectLocalRepositoryImpl
import com.example.xsuniaik.models.local.LocalRecipe


class FilteredScreenViewModel(
    private val localRepo: IProjectLocalRepository
) : BaseViewModel() {


    fun getAllFilteredRecipes(): LiveData<MutableList<LocalRecipe>> = localRepo.getAllFilteredRecipes()
    var list = hashSetOf<LocalRecipe>()

    fun filter(product: String, listForFilter: MutableList<LocalRecipe>?): List<LocalRecipe> {
        var list = arrayListOf<LocalRecipe>()
        listForFilter?.forEach { it ->
            val allIngridients: String = ""
            if (it.sections != null) {
                it.sections!!.forEach {
                    allIngridients.plus(it)
                }
                if (allIngridients.contains(product)) {
                    list.add(it)
                }
            }
        }
        return list
    }
}
