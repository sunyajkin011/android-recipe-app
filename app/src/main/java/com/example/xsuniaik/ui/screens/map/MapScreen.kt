package com.example.xsuniaik.ui.screens.map

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.example.xsuniaik.R
import com.example.xsuniaik.map.ClusterMapRender
import com.example.xsuniaik.map.MarkerUtil

import com.example.xsuniaik.models.ScreenState
import com.example.xsuniaik.models.Store
import com.example.xsuniaik.navigation.INavigationRouter
import com.example.xsuniaik.ui.screens.detail.RecipeDetailScreenContent
import com.example.xsuniaik.utils.Constants
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.algo.GridBasedAlgorithm
import com.google.maps.android.compose.*
import cz.mendelu.pef.compose.petstore.ui.elements.BackArrowScreen
import cz.mendelu.pef.compose.petstore.ui.elements.ErrorScreen
import cz.mendelu.pef.compose.petstore.ui.elements.LoadingScreen
import org.koin.androidx.compose.getViewModel

@Composable
fun MapScreen(
    navigation: INavigationRouter, viewModel: MapViewModel = getViewModel()
) {

    val screenState: MutableState<ScreenState<List<Store>>> = rememberSaveable {
        mutableStateOf(ScreenState.Loading)
    }

    viewModel.storeUiState.value.let {
        when (it) {
            is MapUiState.Error ->{ screenState.value = ScreenState.Error(it.error)}
            is MapUiState.Start -> {
                LaunchedEffect(it){
                    viewModel.getData()
                }
            }
            is MapUiState.Success -> {
                screenState.value = ScreenState.DataLoaded(it.data)
            }
        }
    }

    Scaffold(
        topBar = {
            BackArrowScreen(
                topBarText = stringResource(id = R.string.map),
                onBackClick = { navigation.returnBack() },
            )
        },
        content = {
            MapScreenContent(screenState = screenState.value, navigation = navigation)
        }
    )


}

@Composable
fun MapScreenContent(
    screenState: ScreenState<List<Store>>,
    navigation: INavigationRouter
) {
    screenState.let {
        when (it) {
            is ScreenState.DataLoaded -> ScreenDetail(result = it.data)
            is ScreenState.Error -> ErrorScreen(text = stringResource(id = it.error), navigation = navigation)
            ScreenState.Loading -> LoadingScreen()
        }
    }
}

@OptIn(MapsComposeExperimentalApi::class)
@Composable
fun ScreenDetail(result: List<Store>) {

    val mapUiSettings by remember {
        mutableStateOf(
            MapUiSettings(
                zoomControlsEnabled = false,
                mapToolbarEnabled = false
            )
        )
    }
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(LatLng(49.62352578743681, 15.346186434122867), 5f)
    }

    val context = LocalContext.current
    var clusterManager by remember { mutableStateOf<ClusterManager<Store>?>(null) }
    var clusterRenderer by remember { mutableStateOf<ClusterMapRender?>(null) }
    var currentMarker by remember { mutableStateOf<Marker?>(null) }

    if (!result.isEmpty()) {
        clusterManager?.addItems(result)
        clusterManager?.cluster()
    }

    Box(Modifier.fillMaxSize()) {
        GoogleMap(
            modifier = Modifier.fillMaxHeight(),
            uiSettings = mapUiSettings,
            cameraPositionState = cameraPositionState
        ) {

            MapEffect(result) { map ->
                if (clusterManager == null) {
                    clusterManager = ClusterManager<Store>(context, map)
                }

                if (clusterRenderer == null) {
                    clusterRenderer = ClusterMapRender(context, map, clusterManager!!)
                }

                clusterManager?.apply {
                    renderer = clusterRenderer
                    algorithm = GridBasedAlgorithm()

                    renderer.setOnClusterItemClickListener { item ->
                        currentMarker = clusterRenderer?.getMarker(item)
                        currentMarker!!.setIcon(
                            BitmapDescriptorFactory
                                .fromBitmap(
                                    MarkerUtil.createCustomMarkerFromLayout(
                                        context,
                                        item!!,
                                        true
                                    )
                                )
                        )

                        true
                    }
                }

                map.setOnCameraIdleListener { clusterManager?.cluster() }
            }
        }

    }
}