package com.example.xsuniaik.ui.screens.map

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.example.xsuniaik.R
import com.example.xsuniaik.architecture.BaseViewModel
import com.example.xsuniaik.architecture.CommunicationResult
import com.example.xsuniaik.communication.IRemoteRepository
import com.example.xsuniaik.communication.RemoteRepositoryImpl
import com.example.xsuniaik.models.Store

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MapViewModel(private val remoteRepository: IRemoteRepository) : BaseViewModel() {

    val storeUiState: MutableState<MapUiState<List<Store>>> =
        mutableStateOf(MapUiState.Start())

    fun getData(){
        launch {
            val result1 = withContext(Dispatchers.IO) {
                remoteRepository.getStores()
            }
                if(result1 is CommunicationResult.Error){
                    storeUiState.value = MapUiState.Error(R.string.failed)
                }
                else if(result1 is CommunicationResult.Exception){
                    storeUiState.value = MapUiState.Error(R.string.no_internet_connection)
                }
                else if(result1 is CommunicationResult.Success){
                    storeUiState.value = MapUiState.Success(result1.data)
                }

        }

    }
}