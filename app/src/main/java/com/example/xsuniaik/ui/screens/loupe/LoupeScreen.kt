package com.example.xsuniaik.ui.screens.loupe

import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.ButtonColors
import androidx.compose.material3.*
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import com.example.xsuniaik.R
import com.example.xsuniaik.navigation.INavigationRouter
import com.example.xsuniaik.ui.theme.GreenyGreen
import com.example.xsuniaik.utils.Constants.TT_ANALYZE_BUTTON
import com.example.xsuniaik.utils.Constants.TT_DETECTED_OBJECTS
import com.example.xsuniaik.utils.Constants.TT_PICK_PHOTO
import com.example.xsuniaik.utils.bitmap
import cz.mendelu.pef.compose.petstore.ui.elements.BackArrowScreen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun LoupeScreen(navigation: INavigationRouter, viewModel: LoupeScreenViewModel = getViewModel()) {
    val sheetState = rememberBottomSheetState(initialValue = BottomSheetValue.Collapsed)
    val scaffoldState = rememberBottomSheetScaffoldState(bottomSheetState = sheetState)
    val scope = rememberCoroutineScope()

    BottomSheetScaffold(
        topBar = {
            BackArrowScreen(
                topBarText =  stringResource(id = R.string.search),
                actions = {}
            ) { navigation.returnBack() }
        },
        scaffoldState = scaffoldState,
        backgroundColor = androidx.compose.material.MaterialTheme.colors.background,
        sheetBackgroundColor = androidx.compose.material.MaterialTheme.colors.background,
        sheetPeekHeight = 0.dp,
        sheetShape = RoundedCornerShape(20.dp),
        sheetContent = {
            IdentifiedCategorySheet(
                navigation = navigation,
                sheetState = sheetState,
                scope = scope,
                viewModel = viewModel
            )

        },
        content = {
            GalleryContent(
                sheetState = sheetState, scope = scope, viewModel = viewModel
            )
        })
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun GalleryContent(
    sheetState: BottomSheetState, scope: CoroutineScope, viewModel: LoupeScreenViewModel
) {
    var imageUri by remember { mutableStateOf<Uri?>(null) }

    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.GetContent()) {
        imageUri = it
    }

    val bitmap by bitmap(imageUri = imageUri)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .verticalScroll(state = rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
            Button(
                modifier = Modifier
                    .height(60.dp)
                    .testTag(TT_PICK_PHOTO)
                    .fillMaxWidth()
                    .padding(8.dp),
                shape = RoundedCornerShape(40),
                colors = ButtonDefaults.buttonColors(colorResource(id = R.color.unselect)),
                onClick = { launcher.launch("image/*") }
            ) {
                Text(text = stringResource(R.string.pick_photo), color=Color.Black)
            }

        if (bitmap != null) {
            Image(
                bitmap = bitmap!!.asImageBitmap(),
                contentDescription = null,
                modifier = Modifier
                    .height(400.dp),
                contentScale = ContentScale.Inside
            )

            Button(
                modifier = Modifier
                    .testTag(TT_ANALYZE_BUTTON)
                    .height(60.dp)
                    .fillMaxWidth()
                    .padding(8.dp),
                shape = RoundedCornerShape(40),
                colors = ButtonDefaults.buttonColors(colorResource(id = R.color.unselect)),
                onClick = {
                    if (bitmap != null) {
                        viewModel.getImageLabels(bitmap!!)
                        scope.launch { sheetState.expand() }
                    }
                }
            ) {
                Text(text = stringResource(R.string.analyze), color=Color.Black)
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun IdentifiedCategorySheet(
    navigation: INavigationRouter,
    sheetState: BottomSheetState,
    scope: CoroutineScope,
    viewModel: LoupeScreenViewModel
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(400.dp), elevation = 20.dp
    ) {
        Column {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp)
            ) {
                Text(
                    text = stringResource(R.string.machine_identifies),
                    color = Color(0xFF777777),
                    modifier = Modifier
                        .align(Alignment.CenterStart)
                        .padding(start = 12.dp)
                )
            }

            LazyColumn(modifier = Modifier.testTag(TT_DETECTED_OBJECTS)) {
                viewModel.detectedObjects.forEach {
                    item(key = it.hashCode()) {
                        val product = it.text
                        ClickableText(
                            text = AnnotatedString(product) ,
                            onClick = {
                                navigation.navigateToFilterScreen(product)
                            })
                    }
                }
            }
        }
    }
}
