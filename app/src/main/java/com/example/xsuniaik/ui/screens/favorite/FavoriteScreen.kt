package com.example.xsuniaik.ui.screens.favorite

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.TopAppBar
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.xsuniaik.R
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.example.xsuniaik.navigation.INavigationRouter
import com.example.xsuniaik.utils.Constants
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FavoriteScreen(
    navigation: INavigationRouter,
    viewModel: FavoriteScreenViewModel = getViewModel()
) {

    val favoriteRecipe = viewModel.getAllFavorite().observeAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(align = Alignment.CenterHorizontally)
                    ) {
                        Text(
                            text = stringResource(id = R.string.app_name),
                            style = MaterialTheme.typography.bodyMedium,
                            color = Color.Black,
                            modifier = Modifier
                                .padding(start = 0.dp)
                                .weight(1.5f)
                        )
                    }
                },
                elevation = 0.dp,
                backgroundColor = MaterialTheme.colorScheme.background
            )
        },
        content = {
            if(favoriteRecipe.value != null){
                PetsListScreenContent(
                    paddingValues = it,
                    navigation = navigation,
                    favoriteRecipe = favoriteRecipe.value!!
                )
            }
        }
    )

}

@Composable
fun PetsListScreenContent(
    paddingValues: PaddingValues,
    navigation: INavigationRouter,
    favoriteRecipe: List<FavoriteRecipe>){

    PetsList(
                paddingValues = paddingValues,
                navigation = navigation,
                recipes = favoriteRecipe
            )
        }

@Composable
fun PetsList(
    paddingValues: PaddingValues,
    navigation: INavigationRouter,
    recipes: List<FavoriteRecipe>
){
    LazyColumn(modifier = Modifier.padding(paddingValues).testTag(Constants.TT_FAVORITE_LIST)) {
        recipes.forEach {
            item(key = it.name) {
                PetRow(
                    recipe = it,
                    onRowClick = {
                        navigation.navigateToRecipeDetail(it.id)
                    }
                )
            }
        }
    }
}

@Composable
fun PetRow(recipe: FavoriteRecipe,
           onRowClick: () -> Unit){
    Box(
        modifier = Modifier.padding(16.dp)
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onRowClick),
            shape = RoundedCornerShape(15.dp),
            elevation = CardDefaults.cardElevation(5.dp)
        ) {
            Box(
                modifier = Modifier.height(200.dp)
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        if (!recipe.thumbnailUrl.contains("http"))
                            "https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg"
                        else
                            recipe.thumbnailUrl
                    ), contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )
                Box(modifier = Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            colors = listOf(
                                Color.Transparent,
                                Color.Black
                            ),
                            startY = 300f
                        )
                    ))
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(12.dp),
                    contentAlignment = Alignment.BottomStart
                ) {
                    Text(recipe.name, style = TextStyle(color = Color.White, fontSize = 16.sp))
                }
            }
        }
    }
}