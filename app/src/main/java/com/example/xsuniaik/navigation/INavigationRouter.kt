package com.example.xsuniaik.navigation

import androidx.navigation.NavController
import com.example.xsuniaik.models.Recipe

interface INavigationRouter {
    fun getNavController(): NavController
    fun returnBack()
    fun navigateToMainScreen()
    fun navigateToRecipeDetail(id: Long)
    fun navigateToLoupeScreen()
    fun navigateToMapScreen()
    fun navigateToFavoriteScreen()
    fun navigateToFilterScreen(product: String)
    fun navigateToAuthScreen()
}