package com.example.xsuniaik.navigation

import androidx.navigation.NavController
import com.example.xsuniaik.models.Recipe

class NavigationRouterImpl(private val navController: NavController) : INavigationRouter {
    override fun getNavController(): NavController = navController

    override fun returnBack() {
        navController.popBackStack()
    }

    override fun navigateToMainScreen() {
        navController.navigate(Destination.MainScreen.route)
    }

    override fun navigateToRecipeDetail(id: Long) {
        navController.navigate(Destination.RecipeDetailScreen.route + "/" +id)
    }

    override fun navigateToLoupeScreen() {
        navController.navigate(Destination.LoupeScreen.route)
    }

    override fun navigateToMapScreen() {
        navController.navigate(Destination.MapScreen.route)
    }

    override fun navigateToFavoriteScreen() {
        navController.navigate(Destination.FavoriteScreen.route)
    }

    override fun navigateToFilterScreen(product: String) {
        navController.navigate(Destination.FilteredScreen.route + "/" + product)
    }

    override fun navigateToAuthScreen() {
        navController.navigate(Destination.AuthScreen.route)
    }

}