package com.example.xsuniaik.navigation

sealed class Destination(
    val route: String
) {
    object AuthScreen : Destination(route = "auth_screen")
    object MainScreen : Destination(route = "home")
    object RecipeDetailScreen: Destination(route = "recipe_detail")
    object LoupeScreen: Destination(route = "loupe")
    object MapScreen: Destination(route = "map_screen")
    object FavoriteScreen: Destination(route = "heart")
    object FilteredScreen:Destination(route ="filter")
}