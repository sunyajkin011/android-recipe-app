package com.example.xsuniaik.navigation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.xsuniaik.utils.Constants
import com.example.xsuniaik.ui.screens.auth.AuthScreen
import com.example.xsuniaik.ui.screens.loupe.LoupeScreen
import com.example.xsuniaik.ui.screens.main.MainScreen
import com.example.xsuniaik.ui.screens.detail.RecipeDetail
import com.example.xsuniaik.ui.screens.favorite.FavoriteScreen
import com.example.xsuniaik.ui.screens.filteredRecipe.FilteredScreen
import com.example.xsuniaik.ui.screens.map.MapScreen

@Composable
fun NavGraph(
    navController: NavHostController = rememberNavController(),
    navigation: INavigationRouter = remember { NavigationRouterImpl(navController) },
    startDestination: String
) {

    NavHost(
        navController = navController,
        startDestination = startDestination
    ) {
        composable(Destination.AuthScreen.route) {
            AuthScreen(navigation = navigation)
        }

        composable(Destination.MainScreen.route) {
           MainScreen(navigation = navigation)
        }

        composable(Destination.RecipeDetailScreen.route + "/{id}",
            arguments = listOf(
                navArgument(Constants.ID) {
                    type = NavType.LongType
                    defaultValue = -1L
                }
            )
        ) {
            val id = it.arguments?.getLong(Constants.ID)
            RecipeDetail(navigation = navigation, id = id!!)
        }

        composable(Destination.LoupeScreen.route) {
            LoupeScreen(navigation = navigation)
        }

        composable(Destination.MapScreen.route) {
            MapScreen(navigation = navigation)
        }

        composable(Destination.AuthScreen.route) {
            AuthScreen(navigation = navigation)
        }

        composable(Destination.FavoriteScreen.route) {
            FavoriteScreen(navigation = navigation)
        }

        composable(Destination.FilteredScreen.route + "/{product}",
            arguments = listOf(
                navArgument(Constants.PRODUCT){
                    type = NavType.StringType
                    defaultValue = ""
                }
            )
        ) {
           val product = it.arguments?.getString(Constants.PRODUCT)
            FilteredScreen(navigation = navigation, product = product!!)
        }
    }
}
