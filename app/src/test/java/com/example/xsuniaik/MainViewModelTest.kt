package com.example.xsuniaik

import com.example.xsuniaik.communication.MockRemoteRepository
import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.ui.screens.main.MainScreenUIState
import com.example.xsuniaik.ui.screens.main.MainScreenViewModel
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Test


@OptIn(ExperimentalCoroutinesApi::class)
class MainViewModelTest {
    @OptIn(DelicateCoroutinesApi::class)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @Test
    fun loadData_isCorrect(): Unit = runTest {
        //given
        var result: ApiResponse? = null
        val mainViewModel = MainScreenViewModel(MockRemoteRepository())

        //when
        withContext(Dispatchers.IO) { mainViewModel.loadRecipes() }

        mainViewModel.mainScreenlUIState.value.let {
            when (it) {
                is MainScreenUIState.Start -> assertTrue(false)
                is MainScreenUIState.Error -> assertTrue(false)
                is MainScreenUIState.Success -> result = it.data
            }
        }

        //then
        Assert.assertEquals(
            ApiResponse(
                count = 2,
                results = listOf(
                    Recipe(
                        1,
                        name = "Potato",
                        cook_time_minutes = 25,
                        instractions = emptyList(),
                        thumbnail_url = "",
                        sections =emptyList()
                    ), Recipe(
                        2,
                        name = "Chicken",
                        cook_time_minutes = 30,
                        instractions = emptyList(),
                        thumbnail_url = "",
                        sections =emptyList()
                    )
                )
            ), result
        )
    }


    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }
}