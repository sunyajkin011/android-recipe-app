package com.example.xsuniaik

import androidx.compose.runtime.mutableStateListOf
import com.example.xsuniaik.communication.db.MockLocalRepository
import com.example.xsuniaik.models.local.LocalRecipe
import com.example.xsuniaik.ui.screens.filteredRecipe.FilteredScreenViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class FilteredViewModelTest {

    @OptIn(DelicateCoroutinesApi::class)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @Test
    fun loadData_isCorrect(): Unit = runTest {
        //given
        var result: List<LocalRecipe>? = null
        val filteredViewModel = FilteredScreenViewModel(MockLocalRepository())

        //when
        result = filteredViewModel.getAllFilteredRecipes().value

        //then
        Assert.assertEquals(2, result!!.size)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }
}