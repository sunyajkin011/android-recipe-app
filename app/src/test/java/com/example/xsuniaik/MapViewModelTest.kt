package com.example.xsuniaik

import androidx.lifecycle.LiveData
import com.example.xsuniaik.communication.MockRemoteRepository
import com.example.xsuniaik.communication.db.MockLocalRepository
import com.example.xsuniaik.models.ApiResponse
import com.example.xsuniaik.models.Recipe
import com.example.xsuniaik.models.Store
import com.example.xsuniaik.models.local.FavoriteRecipe
import com.example.xsuniaik.ui.screens.favorite.FavoriteScreenViewModel
import com.example.xsuniaik.ui.screens.map.MapUiState
import com.example.xsuniaik.ui.screens.map.MapViewModel
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test


@OptIn(ExperimentalCoroutinesApi::class)
class MapViewModelTest {



    @OptIn(DelicateCoroutinesApi::class)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @Test
    fun loadData_isCorrect(): Unit = runTest {
        //given
        var result: List<Store>? = null
        val mapViewModel = MapViewModel(MockRemoteRepository())

        //when
        withContext(Dispatchers.IO) { mapViewModel.getData() }

        mapViewModel.storeUiState.value.let {
            when (it) {
                is MapUiState.Start -> assertTrue(false)
                is MapUiState.Error -> assertTrue(false)
                is MapUiState.Success -> result = it.data
            }
        }

        //then
        Assert.assertEquals(
            listOf(
                Store(
                    1,
                    name = "Sturbaks",
                    address = "Kohoutova 9",
                    latitude = 23.2,
                    longitude = 56.9,
                    type = "food"
                ), Store(
                    2,
                    name = "Sturbaks",
                    address = "Namesti Svobody 17",
                    latitude = 23.4,
                    longitude = 56.5,
                    type = "food"
                )
            ), result
        )
    }


    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

}